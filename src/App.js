import React from "react";
import {Switch, BrowserRouter, Route} from 'react-router-dom';
import Admin from './Pages/Admin';


import './styles/Main.scss';

const App = () => {

    return (
        <BrowserRouter>
            <Switch>
                <Route path={'/'} exact={true} component={Admin}/>   
            </Switch>
        </BrowserRouter>
    );
}

export default App;
