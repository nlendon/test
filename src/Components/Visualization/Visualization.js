import React, { useState, useEffect } from "react";
import Chart from "../../Visualization/Chart";
import Tab from "../../Visualization/Tools/Tab/Tab";

const Visualization = () => {
  const [tab, setTab] = useState(false);
  const [id, setId] = useState("");

  useEffect(() => Chart(setTab, setId), []);

  return (
    <div style={{height: "100v"}}>
        <div id={"container"} />
      {tab && <Tab id={id} setId={setId} setTab={setTab} tab={tab} />}
    </div>
  );
};

export default Visualization;
