import React, { Fragment } from 'react';
import { Button, Form, Input, InputNumber, Popover } from "antd";

const TabComponent = ({ data }) => {
    const onFinish = (values) => {
        console.log(values);
    };
    const contentHayr = (
        <div>
            <Form
                name="nest-messages"
                onFinish={onFinish}
                style={{ maxWidth: 600 }}
            >
                <Form.Item name={['user', 'name']} label="Name" rules={[{ required: true }]}>
                    <Input />
                </Form.Item>
                <Form.Item name={['user', 'age']} label="Age" rules={[{ type: 'number', min: 0, max: 99 }]}>
                    <InputNumber />
                </Form.Item>

                <Form.Item wrapperCol={{ offset: 8 }}>
                    <Button type="primary" htmlType="submit">
                        Submit
                    </Button>
                </Form.Item>
            </Form>
        </div>
    );
    const contentMayr = (
        <div>
            <Form
                name="nest-messages"
                onFinish={onFinish}
                style={{ maxWidth: 600 }}
            >
                <Form.Item name={['user', 'name']} label="Name" rules={[{ required: true }]}>
                    <Input />
                </Form.Item>
                <Form.Item name={['user', 'age']} label="Age" rules={[{ type: 'number', min: 0, max: 99 }]}>
                    <InputNumber />
                </Form.Item>

                <Form.Item wrapperCol={{ offset: 8 }}>
                    <Button type="primary" htmlType="submit">
                        Submit
                    </Button>
                </Form.Item>
            </Form>
        </div>
    );
    const contentVordi = (
        <div>
            <Form
                name="nest-messages"
                onFinish={onFinish}
                style={{ maxWidth: 600 }}
            >
                <Form.Item name={['user', 'name']} label="Name" rules={[{ required: true }]}>
                    <Input />
                </Form.Item>
                <Form.Item name={['user', 'age']} label="Age" rules={[{ type: 'number', min: 0, max: 99 }]}>
                    <InputNumber />
                </Form.Item>

                <Form.Item wrapperCol={{ offset: 8 }}>
                    <Button type="primary" htmlType="submit">
                        Submit
                    </Button>
                </Form.Item>
            </Form>
        </div>
    );
    const contentDustr = (
        <div>
            <Form
                name="nest-messages"
                onFinish={onFinish}
                style={{ maxWidth: 600 }}
            >
                <Form.Item name={['user', 'name']} label="Name" rules={[{ required: true }]}>
                    <Input />
                </Form.Item>
                <Form.Item name={['user', 'age']} label="Age" rules={[{ type: 'number', min: 0, max: 99 }]}>
                    <InputNumber />
                </Form.Item>

                <Form.Item wrapperCol={{ offset: 8 }}>
                    <Button type="primary" htmlType="submit">
                        Submit
                    </Button>
                </Form.Item>
            </Form>
        </div>
    );
    return (
        <>
            <Popover content={contentHayr} title="" trigger="click">
                <Button>avelacnel hayr</Button>
            </Popover>
            <Popover content={contentMayr} title="" trigger="click">
                <Button>avelacnel mayr</Button>
            </Popover>
            <Popover content={contentDustr} title="" trigger="click">
                <Button>avelacnel vordi</Button>
            </Popover>
            <Popover content={contentVordi} title="" trigger="click">
                <Button>avelacnel dustr</Button>
            </Popover>

        </>
    )
}

export default TabComponent;