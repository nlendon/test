import React from 'react';
import {Drawer} from 'antd';
import TabComponent from "./TabComponent";

const Tab = ({id, setId, setTab, tab}) => {

    return (
        <>
            <Drawer placement="right" onClose={() => setTab(false)} open={tab}
                    style={{textAlign: 'center'}}>
                <TabComponent />
            </Drawer>
        </>
    );
};
export default Tab;
